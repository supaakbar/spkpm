<?php
    require_once '../koneksi/conn.php';
    session_start();
    if (isset($_POST['submitform'])) {

            $jumlah = 1;
            $nama = array();

            $nilaiipk = array();
            $textipk = array();
            $gapipk = array();
            $bobotipk = array();

            $nilaipenghasilan = array();
            $textpenghasilan = array();
            $gappenghasilan = array();
            $bobotpenghasilan = array();

            $nilaitanggungan = array();
            $texttanggungan = array();
            $gaptanggungan = array();
            $bobottanggungan = array();

            $nilaismt = array();
            $textsmt = array();
            $gapsmt = array();
            $bobotsmt = array();

            $ncfsiswa = array();
            $nsfsiswa = array();
            $hasilsiswa = array();

            for($a=1;$a<=$jumlah;$a++) {

               $nama[$a] = $_POST['namasiswa'.$a];
               $nilaiipk[$a] = $_POST['ipk'.$a];
               $nilaipenghasilan[$a] = $_POST['penghasilan'.$a];
               $nilaitanggungan[$a] = $_POST['tanggungan'.$a];
               $nilaismt[$a] = $_POST['smt'.$a];

               $sql = $conn->query("INSERT INTO siswa (nama, ipk, penghasilan, tanggungan, semester) VALUES('$nama[$a]','$nilaiipk[$a]','$nilaipenghasilan[$a]','$nilaitanggungan[$a]','$nilaismt[$a]')");

            }

            for($a=1;$a<=$jumlah;$a++) {

                if ($nilaiipk[$a] == "1"){
                    $textipk[$a] = "< 2,5 ";
                } elseif ($nilaiipk[$a] == "2") {
                    $textipk[$a] = ">2,5 dan <= 3";
                } elseif ($nilaiipk[$a] == "3") {
                    $textipk[$a] = ">3 dan <= 3.,5";
                } else {
                    $textipk[$a] = "> 3,5";
                }

                if ($nilaipenghasilan[$a] == "4"){
                    $textpenghasilan[$a] = "<=1.000.000";
                } elseif ($nilaipenghasilan[$a] == "3") {
                    $textpenghasilan[$a] = ">1.000.000 - <=3.000.000";
                } elseif ($nilaipenghasilan[$a] == "2") {
                    $textpenghasilan[$a] = ">3.000.000 - <=5.000.000";
                } else {
                    $textpenghasilan[$a] = ">=5.000.000";
                }

                if ($nilaitanggungan[$a] == "1"){
                    $texttanggungan[$a] = "Jumlah 1";
                } elseif ($nilaitanggungan[$a] == "2") {
                    $texttanggungan[$a] = "Jumlah 2";
                } elseif ($nilaitanggungan[$a] == "3") {
                    $texttanggungan[$a] = "Jumlah 3";
                } else {
                    $texttanggungan[$a] = "Jumlah >3";
                }

                if ($nilaismt[$a] == "0"){
                    $textsmt[$a] = "<=2 / >8";
                } elseif ($nilaismt[$a] == "1") {
                    $textsmt[$a] = "3";
                } elseif ($nilaismt[$a] == "2") {
                    $textsmt[$a] = "4";
                } elseif ($nilaismt[$a] == "3") {
                    $textsmt[$a] = "5, 6";
                } else {
                    $textsmt[$a] = "7, 8";
                }

                $sql = $conn->query("INSERT INTO keterangansiswa (nama, ket_ipk, ket_penghasilan, ket_tanggungan, ket_smt) VALUES('$nama[$a]','$textipk[$a]','$textpenghasilan[$a]','$texttanggungan[$a]','$textsmt[$a]')");

            }

            for($a=1;$a<=$jumlah;$a++) {
                
                $nama[$a] = $_POST['namasiswa'.$a];
                $gapipk[$a] = $nilaiipk[$a] - 3;
                $gappenghasilan[$a] = $nilaipenghasilan[$a] - 3;
                $gaptanggungan[$a] = $nilaitanggungan[$a] - 3;
                $gapsmt[$a] = $nilaismt[$a] - 2;

                $sql = $conn->query("INSERT INTO gapsiswa (nama, gapipk, gappenghasilan, gaptanggungan, gapsmt) VALUES('$nama[$a]','$gapipk[$a]','$gappenghasilan[$a]','$gaptanggungan[$a]','$gapsmt[$a]')");

            }

            for($a=1;$a<=$jumlah;$a++) {

                if ($gapipk[$a] == "0"){
                    $bobotipk[$a] = "5";
                } elseif ($gapipk[$a] == "1") {
                    $bobotipk[$a] = "4.5";
                } elseif ($gapipk[$a] == "-1") {
                    $bobotipk[$a] = "4";
                } elseif ($gapipk[$a] == "2") {
                    $bobotipk[$a] = "3.5";
                } elseif ($gapipk[$a] == "-2") {
                    $bobotipk[$a] = "3";
                } elseif ($gapipk[$a] == "3") {
                    $bobotipk[$a] = "2.5";
                } elseif ($gapipk[$a] == "-3") {
                    $bobotipk[$a] = "2";
                } elseif ($gapipk[$a] == "4") {
                    $bobotipk[$a] = "1.5";
                } else {
                    $bobotipk[$a] = "1";
                }

                if ($gappenghasilan[$a] == "0"){
                    $bobotpenghasilan[$a] = "5";
                } elseif ($gappenghasilan[$a] == "1") {
                    $bobotpenghasilan[$a] = "4.5";
                } elseif ($gappenghasilan[$a] == "-1") {
                    $bobotpenghasilan[$a] = "4";
                } elseif ($gappenghasilan[$a] == "2") {
                    $bobotpenghasilan[$a] = "3.5";
                } elseif ($gappenghasilan[$a] == "-2") {
                    $bobotpenghasilan[$a] = "3";
                } elseif ($gappenghasilan[$a] == "3") {
                    $bobotpenghasilan[$a] = "2.5";
                } elseif ($gappenghasilan[$a] == "-3") {
                    $bobotpenghasilan[$a] = "2";
                } elseif ($gappenghasilan[$a] == "4") {
                    $bobotpenghasilan[$a] = "1.5";
                } else {
                    $bobotpenghasilan[$a] = "1";
                }

                if ($gaptanggungan[$a] == "0"){
                    $bobottanggungan[$a] = "5";
                } elseif ($gaptanggungan[$a] == "1") {
                    $bobottanggungan[$a] = "4.5";
                } elseif ($gaptanggungan[$a] == "-1") {
                    $bobottanggungan[$a] = "4";
                } elseif ($gaptanggungan[$a] == "2") {
                    $bobottanggungan[$a] = "3.5";
                } elseif ($gaptanggungan[$a] == "-2") {
                    $bobottanggungan[$a] = "3";
                } elseif ($gaptanggungan[$a] == "3") {
                    $bobottanggungan[$a] = "2.5";
                } elseif ($gaptanggungan[$a] == "-3") {
                    $bobottanggungan[$a] = "2";
                } elseif ($gaptanggungank[$a] == "4") {
                    $bobottanggungan[$a] = "1.5";
                } else {
                    $bobottanggungan[$a] = "1";
                }

                if ($gapsmt[$a] == "0"){
                    $bobotsmt[$a] = "5";
                } elseif ($gapsmt[$a] == "1") {
                    $bobotsmt[$a] = "4.5";
                } elseif ($gapsmt[$a] == "-1") {
                    $bobotsmt[$a] = "4";
                } elseif ($gapsmt[$a] == "2") {
                    $bobotsmt[$a] = "3.5";
                } elseif ($gapsmt[$a] == "-2") {
                    $bobotsmt[$a] = "3";
                } elseif ($gapsmt[$a] == "3") {
                    $bobotipk[$a] = "2.5";
                } elseif ($gapsmt[$a] == "-3") {
                    $bobotsmt[$a] = "2";
                } elseif ($gapsmt[$a] == "4") {
                    $bobotsmt[$a] = "1.5";
                } else {
                    $bobotsmt[$a] = "1";
                }

                $ncfsiswa[$a] = (($bobotipk[$a]) + ($bobotpenghasilan[$a]))/2;
                $nsfsiswa[$a] = (($bobottanggungan[$a]) + ($bobotsmt[$a]))/2;
                $hasilsiswa[$a] = (0.6 * $ncfsiswa[$a]) + (0.4 * $nsfsiswa[$a]);

                $sql = $conn->query("INSERT INTO hasilsiswa (nama, bobotipk, bobotpenghasilan, bobottanggungan, bobotsmt, ncf, nsf, hasil) VALUES('$nama[$a]','$bobotipk[$a]','$bobotpenghasilan[$a]','$bobottanggungan[$a]','$bobotsmt[$a]','$ncfsiswa[$a]','$nsfsiswa[$a]','$hasilsiswa[$a]')");

            }
        }

    if ($sql) {
        echo "<script>alert('Berhasil di tambahkan');window.location='http://localhost/SPKPM_1/#record'</script>";
    }
?>